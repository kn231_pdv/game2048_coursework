﻿using System.Drawing;
using System.Windows.Forms;

namespace Game2048
{
    class Score : Panel 
    {
        public Score(string title, int initialValue = 0)
        {
            BackColor = Color.FromArgb(128,158,191);
            Width = WidthValue;
            Height = HeightValue;

            _Value = initialValue;

            _TitleLabel = new Label() 
            {
                Text = title,
                Font = new Font("Comic Sans", 16, FontStyle.Bold),
                Size = new Size(WidthValue, HeightValue / 2),
                Location = new Point(0, 0),
                ForeColor = Color.FromArgb(255,255,255), 
                TextAlign = ContentAlignment.MiddleCenter
            };

            _ValueLabel = new Label() 
            {
                Text = $"{_Value}",
                Font = new Font("Comic Sans", 20, FontStyle.Bold),
                Size = new Size(WidthValue, HeightValue / 2),
                Location = new Point(0, HeightValue / 2),
                ForeColor = Color.FromArgb(255, 255, 255), 
                TextAlign = ContentAlignment.MiddleCenter
            };

            Controls.Add(_TitleLabel);
            Controls.Add(_ValueLabel); 
        }
        /// <summary>
        /// Встановлює рахунок.
        /// </summary>
        /// <param name="value">Значення рахунку.</param>

        public void SetValue(int value) 
        {
            _Value = value;
            _ValueLabel.Text = $"{_Value}";
        }
        /// <summary>
        /// Збільшує рахунок на передане значення.
        /// </summary>
        /// <param name="value">Значення, на яке потрібно збільшити рахунок.</param>

        public void Increase(int value) 
        {
            SetValue(_Value + value);
        }

        /// <summary>
        /// Скидає рахунок.
        /// </summary>
        public void Reset() 
        {
            SetValue(0);
        }

        public int Value
        { //Властивість для отримання поточного значення рахунку.
            get
            {
                return _Value;
            }
        }

        public readonly static int WidthValue = 95;

        public readonly static int HeightValue = 70;

        private Label _TitleLabel; 

        private Label _ValueLabel;

        private int _Value;
    }
}