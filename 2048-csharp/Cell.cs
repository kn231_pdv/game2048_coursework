﻿using System.Drawing; 
using System.Windows.Forms;

namespace Game2048
{
    class Cell : Label 
    {
        public Cell(int x, int y) 
        {
            Size = new Size(SizeValue, SizeValue);
            Location = new Point(x, y);
            Font = new Font("Comic Sans", 24, FontStyle.Bold);
            TextAlign = ContentAlignment.MiddleCenter;
        }
        /// <summary>
        /// Встановлює кольори для клітинки.
        /// </summary>
        public CellColor Style
        {
            set //сетер для властивості Style, який визначає, що відбувається при встановленні значення цієї властивості.
            {
                ForeColor = value.Foreground; 
                BackColor = value.Background; 
            }
        }

        public readonly static int SizeValue = 110; 

        public readonly static int MarginValue = 10; 
    }
}
