﻿using System;
using System.IO;
using System.IO.IsolatedStorage;

namespace Game2048
{
    public class Storage 
    {
        public Storage()
        {
            _Storage = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly,null,null);
        }
        /// <summary>
        /// Отримує найкращий рахунок з сховища додатка.
        /// </summary>
        /// <returns>
        /// Повертає найкращий рахунок з сховища додатка. Якщо рахунок отримати з сховища не вдалося, то повертає 0.
        /// </returns>
        public int ReadBestScore() 
        {
            int score;
            StreamReader sr = null; 

            try
            {
                sr = new StreamReader(new IsolatedStorageFileStream("Data\\bestScore.txt", FileMode.Open, _Storage));
                score = Convert.ToInt32(sr.ReadLine());
                sr.Close();
            }
            catch
            {
                score = 0;
            }
            return score;
        }
        /// <summary>
        /// Записує значення найкращого рахунку до сховища.
        /// </summary>
        /// <param name="score">Значення найкращого рахунку.</param>

        public void WriteBestScore(int score) 
        {
            _Storage.CreateDirectory("Data"); 
            using (StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream("Data\\bestScore.txt", FileMode.Create, _Storage)))
            {
                sw.WriteLine(score); 
            }
        }
        private readonly IsolatedStorageFile _Storage;
    }
}